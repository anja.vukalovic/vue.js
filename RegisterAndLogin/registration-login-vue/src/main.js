// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

Vue.config.productionTip = false

const httpClient = axios.create({
  baseURL: 'http://localhost:5000', 
  headers: {
    // 'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
});
// Create a function that checks if there is token in local storage
// If there is no token redirect to login page (check if page is login)
// Make that function immediately invoked one - https://developer.mozilla.org/en-US/docs/Glossary/IIFE
const requestAuthInterceptor = (config) => {
  config.headers["token"] =
    "Read Token from locale storage - If there is no token redirect to login page";
  return config;
};

// Error Interceptor - processing
const responseErrorInterceptor = (error) => {
  const ignoreRoutes = ["Routes that should be ignored when handling errors"];
  // TODO check some and every functions
  let isIgnored = ignoreRoutes.some(
    function (route) {
      return this[0].includes(route);
    },
    [error.response.config.url]
  );
  switch (error.response.status) {
    case 400:
      // parse BAD REQUEST errors
      break;
    case 401: // Unauthorized request
      // Redirect to login and remove token from locale storage
      break;
    case 403: // Forbidden request
      // User is authorized but does not have needed permissions
      break;
    case 500: // Server side errors
      break;
    default:
    // Default behaviour
  }
  return Promise.reject(error);
};

// Interceptor for responses
const responseInterceptor = (response) => {
  const stateChangeMethods = ["post", "put", "patch", "delete"];
  const ignoreRoutes = ["Routes that should be ignored"];
  // Check if URL contains ignoredRoute
  let isIgnored = ignoreRoutes.some(
    function (route) {
      return this[0].includes(route);
    },
    [response.config.url]
  );
  switch (response.status) {
    case 200:
    case 201:
    case 202:
    case 204:
      // What to do when following codes are returned
      // Exp. Notify user that action has been performed successfully
      break;
    default:
  }
  return response;
};
httpClient.interceptors.request.use(requestAuthInterceptor);
httpClient.interceptors.response.use(
  responseInterceptor,
  responseErrorInterceptor
);
export { httpClient };


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

