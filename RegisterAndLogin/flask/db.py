import pymysql.cursors


"""
cursor.execte(query) vraca integer ako nije doslo do greske
  - za select vraca broj redova tabele
  - za insert, update... vraca:
        1 ako se desila izmjena u tabeli (ako se jedna stvar promijenila u tabeli)
        0 ako se nista nije promijenilo u tabeli 
"""


def execute_query(query_type, query, db_name = 'api_i_flask'):
    # Zapocni konekciju
    connection = pymysql.connect(host = 'localhost',
                                user = 'root',
                                password = '8520',
                                database = db_name,
                                charset = 'utf8mb4',
                                cursorclass = pymysql.cursors.DictCursor)
    
    # Pomocne promjenjive
    rezultat = []
    select_upit = (query_type == "select")

    # Izvrsi upit i zatvori konekciju
    with connection:
        with connection.cursor() as cursor:
            br_redova = cursor.execute(query)
            if select_upit:
                rezultat = cursor.fetchall()
        
        if not(select_upit):
            connection.commit()

    return [br_redova, rezultat]


def select(query):
    return execute_query("select", query)

def insert(query):
    return execute_query("insert", query)

def update(query):
    return execute_query("update", query)

def delete_row(query):
    return execute_query("delete", query)

def delete_table(query):
    return execute_query("drop", query)

