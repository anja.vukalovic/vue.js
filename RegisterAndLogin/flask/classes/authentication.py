import pymysql
from flask import Flask, request, jsonify, make_response,render_template
from  werkzeug.security import generate_password_hash, check_password_hash
import jwt
from functools import wraps
# from flask_restful import Resource, Api, reqparse
from datetime import datetime, timedelta
import uuid
from flask_restful import Resource, Api, reqparse
from flask import Flask, jsonify
from flask_restful import Resource,reqparse
import db

app = Flask(__name__)
api = Api(app)

class Login_restaurant(Resource):
    def post(self):
        # creates dictionary of form data
        try:
         
            parse = reqparse.RequestParser()
            print(parse)
            parse.add_argument("naziv", required = True, type = str) 
            parse.add_argument("sifra",required = True, type = str)
            args = parse.parse_args()
            print(args["naziv"])
            if not args or not args['naziv'] or not args['sifra']:
                # returns 401 if any email or / and password is missing
                return make_response(
                    'Could not verify',
                    401,
                    {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
                )
        
            user = db.select(f"select * from Restoran where naziv = '{args['naziv']}' ")
        
            if user[0] == 0:
                # returns 401 if user does not exist
                return make_response(
                    'Could not verify',
                    401,
                    {'WWW-Authenticate' : 'Basic realm ="User does not exist !!"'}
                )
        
            user = user[1][0]

            # generates the JWT Token
            if check_password_hash(user["sifra"],args["sifra"]):
                token = jwt.encode({
                    'sifra': user["sifra"],
                    'naziv': user["naziv"],
                    # 'exp' : datetime.utcnow() + timedelta(minutes = 30)
                }, "key")

            return make_response(jsonify({'token' : token.decode('UTF-8'),'id': user['id'],'naziv': user['naziv']}), 201)
            
        except Exception as e:
            import sys, os
            print('SSSSSSSSSSSSSSSSSSSSss')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, e)
            return make_response(
                'Bad request',
                400,
                {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
            )


class Signup_restaurant(Resource):
    def post(self):
        # creates a dictionary of the form data
        data = reqparse.RequestParser()
    
        # gets name, email and password
        data.add_argument("naziv", type = str) 
        data.add_argument("sifra", required = True, type = str) 
        
        args = data.parse_args()
       

        user = db.select(f"select * from Restoran where naziv = '{args['naziv']}' ");
        print(user)
        if user[0] == 0:
            # database ORM object
            sifra = generate_password_hash(args['sifra'])
            # sifra1 = args["sifra"]
            # sifra2 = generate_password_hash(sifra1)
            db.insert(f'''insert into Restoran(naziv,sifra)
            values("{args["naziv"]}", "{sifra}" )''')
           
            
            return make_response('Successfully registered.', 201)
        else:
            # returns 202 if user already exists
            return make_response('User already exists. Please Log in.', 202)


# decorator for verifying the JWT
def token_required_res(f):
    @wraps(f)
    def decorated(*args, **kwargs): 
        try: 
            token = None
            # jwt is passed in the request header
            if 'r-access-token' in request.headers:
                token = request.headers['r-access-token']
                
            # return 401 if token is not passed
            if not token:
                return make_response(jsonify({'message' : 'Token is missing !!'}), 401)
    
            try:
                # decoding the payload to fetch the stored details
                data = jwt.decode(token, "key")
                
                print("eeeeeeeeeeeeeeeeeeeeeeee")
                print(data)
                current_res = db.select(f"select naziv,sifra from Restoran where naziv = '{data['naziv']}'  ")
                
            except Exception as e: 
                return make_response(jsonify({
                    'message' : 'Token is invalid !!'
                }),401)

            # returns the current logged in users contex to the routes
            return  f(current_res, *args, **kwargs)
    
        except Exception as e:
            import sys, os
            print('SSSSSSSSSSSSSSSSSSSSss')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, e)
            return make_response(
                'Bad request',
                400,
                {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
            )
    return decorated
  
# User Database Route
# this route sends back list of users users
class Restaurant_auth(Resource):
    @token_required_res
    def get(current_res, *args, **kwargs):
        try:
# @app.route('/user', methods =['GET'])
            # querying the database
            # for all the entries in it
            users = db.select("select * from Restoran")
            # converting the query objects
            # to list of jsons
            output = []
            
            users = users[1]
            for user in users:
                print(users)
                print("useeeeeer")
                # appending the user data json
                # to the response list
                output.append({
                    # 'id': user.id,
                    'id' : user["id"],
                    'naziv' : user["naziv"],       
                })
        
            return jsonify({'users': output})
        except Exception as e:
            import sys, os
            print('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, e)
            return make_response(
                'Bad request',
                400,
                {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
            )

if __name__ == "__main__":
    # setting debug to True enables hot reload
    # and also provides a debugger shell
    # if you hit an error while running the server
    app.run(debug = True)
