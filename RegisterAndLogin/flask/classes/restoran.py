from flask_restful import Resource,reqparse
import db


class Restoran(Resource):
    def get(self):
        _,restoran_data = db.select('select * from Restoran')
        return restoran_data

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("naziv", required = True, type = str) 
        parser.add_argument("sifra",required = True, type = str)
        args = parser.parse_args()
        db.insert(f'''insert into Restoran(naziv,sifra)
        values("{args["naziv"]}", "{args["sifra"]}")''')

    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", required = True, type = int)
        args = parser.parse_args()
        db.delete_row(f'delete from Restoran where id = {args["id"]}')

    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", required = True, type = int)
        parser.add_argument("naziv", required = True, type = str) 
        parser.add_argument("sifra",required = True, type = str)
        args = parser.parse_args()

        db.update(f'''update Restoran set naziv="{args["naziv"]}", sifra = "{args["sifra"]}"
        where id = {args["id"]}''')
