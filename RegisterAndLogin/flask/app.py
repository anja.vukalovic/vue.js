from flask import Flask, jsonify
from flask_restful import Api
from flask_cors import CORS

from classes.restoran import Restoran
from classes.authentication import Login_restaurant,Signup_restaurant,Restaurant_auth

app = Flask(__name__)
api = Api(app)

cors = CORS(app)
app.config['CORS-HEADERS'] = 'Content-Type' 


api.add_resource(Restoran, '/restoran')
api.add_resource(Login_restaurant,'/loginasres')
api.add_resource(Signup_restaurant,'/signupasres')
api.add_resource(Restaurant_auth,'/restaurantauth')


if __name__ == '__main__':
    app.run(debug = True)
